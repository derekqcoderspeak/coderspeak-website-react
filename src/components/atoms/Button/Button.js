import styled, { css } from 'styled-components';

const Button = styled.button`
  padding: 0;
  background-color: ${({ theme }) => theme.note};
  border: none;
  font-weight: bold;
  font-size: 15px;
  line-height: 19px;
  text-align: center;
  background-color: #0462b9;
  color: #ffffff;
  border-radius: 24px;
  width: 120px;
  height: 44px;
  ${({ secondary }) =>
    secondary &&
    css`
      width: 256px;
      height: 48px;
      background-color: ${({ theme }) => theme.note};
    `}

  ${({ dark }) =>
    dark &&
    css`
      background: #24242a;
    `}
`;

export default Button;
