import styled from 'styled-components';

const MenuItem = styled.a`
  font-weight: bold;
  font-size: 15px;
  line-height: 19px;
  color: #24242a;
  padding: 0px 8px;
  text-decoration: none;
`;

export default MenuItem;
