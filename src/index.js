import React from 'react';
import ReactDOM from 'react-dom';
import Root from 'views/Root';

//import { Bootstrap, Grid, Row, Col } from 'react-bootstrap';

ReactDOM.render(<Root />, document.getElementById('root'));
